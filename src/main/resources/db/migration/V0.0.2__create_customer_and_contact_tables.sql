create table if not exists address_book.customer(
  id bigint identity primary key,

  first_name varchar(128) not null,
  last_name varchar(128) not null,

  created_at timestamp with time zone not null,
  updated_at timestamp with time zone
);

create table if not exists address_book.contact(
  customer_id bigint not null,
  tag varchar(64) not null,

  phone_number varchar(32) not null,

  created_at timestamp with time zone not null,
  updated_at timestamp with time zone,

  primary key (customer_id, tag),
  foreign key (customer_id) references customer(id),
  unique key contact_customer_and_tag(customer_id, tag)
);
