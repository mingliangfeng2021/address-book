package com.reece.customer.exception;

import com.reece.customer.api.ApiError;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.servlet.http.HttpServletRequest;

@ControllerAdvice
public class GlobalExceptionHandler {

  @ExceptionHandler({ CustomerNotFoundException.class, ContactNotFoundException.class })
  public ResponseEntity<ApiError> entityNotFoundHandler(HttpServletRequest httpServletRequest, RuntimeException exception) {
    return buildApiErrorResponse(exception, HttpStatus.NOT_FOUND);
  }

  @ExceptionHandler({ HttpMessageNotReadableException.class, MethodArgumentNotValidException.class })
  public ResponseEntity<ApiError> badRequest(HttpServletRequest httpServletRequest, Exception exception) {
    return buildApiErrorResponse(exception, HttpStatus.BAD_REQUEST);
  }

  private ResponseEntity<ApiError> buildApiErrorResponse(Exception exception, HttpStatus badRequest) {
    HttpStatus httpStatus = badRequest;
    ApiError apiError = ApiError.builder()
        .status(httpStatus.name())
        .message(exception.getMessage())
        .build();

    return ResponseEntity.status(httpStatus).body(apiError);
  }

}
