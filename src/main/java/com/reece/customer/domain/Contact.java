package com.reece.customer.domain;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.time.OffsetDateTime;

@Entity
@Builder
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@IdClass(ContactPK.class)
public class Contact {

  @Id
  @Column(name = "customer_id")
  private Long customerId;

  @Id
  @Column
  private String tag;

  @Column(name = "phone_number")
  private String phoneNumber;

  @Column(name = "created_at")
  private OffsetDateTime createdAt;

  @Column(name = "updated_at")
  private OffsetDateTime updatedAt;

  @JsonIgnore
  public ContactPK getContactPK() {
    return new ContactPK(customerId, tag);
  }

}
