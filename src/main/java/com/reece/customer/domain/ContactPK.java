package com.reece.customer.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ContactPK implements Serializable {

  private static final long serialVersionUID = -6170895130702052650L;

  private Long customerId;
  private String tag;

}
