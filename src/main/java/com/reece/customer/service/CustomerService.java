package com.reece.customer.service;

import com.reece.customer.api.customer.CustomerRequest;
import com.reece.customer.repository.ContactRepository;
import com.reece.customer.repository.CustomerRepository;
import com.reece.customer.domain.Customer;
import com.reece.customer.exception.CustomerNotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;

@Service
@RequiredArgsConstructor
public class CustomerService {

  private final CustomerRepository customerRepository;
  private final ContactRepository contactRepository;

  public Customer createCustomer(CustomerRequest customerRequest) {
    Customer customer = Customer.builder()
        .createdAt(OffsetDateTime.now())
        .build();

    copyToCustomer(customerRequest, customer);

    return customerRepository.save(customer);
  }

  public Customer updateCustomer(Long customerId, CustomerRequest customerRequest) {
    Customer customer = getCustomer(customerId);
    customer.setUpdatedAt(OffsetDateTime.now());

    copyToCustomer(customerRequest, customer);

    return customerRepository.save(customer);
  }

  public Customer getCustomer(Long customerId) {
    return customerRepository.findById(customerId)
        .orElseThrow(() -> new CustomerNotFoundException("Cannot find customer with customerId: " + customerId));
  }

  public void deleteCustomer(Long customerId) {
    Customer customer = getCustomer(customerId);
    contactRepository.deleteAll(customer.getContacts());
    customer.setContacts(null);
    customerRepository.delete(customer);
  }

  private void copyToCustomer(CustomerRequest customerRequest, Customer customer) {
    customer.setFirstName(customerRequest.getFirstName());
    customer.setLastName(customerRequest.getLastName());
  }

}
