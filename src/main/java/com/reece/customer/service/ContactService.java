package com.reece.customer.service;

import com.reece.customer.api.contact.ContactRequest;
import com.reece.customer.api.contact.ContactsRequest;
import com.reece.customer.domain.Contact;
import com.reece.customer.domain.ContactPK;
import com.reece.customer.domain.Customer;
import com.reece.customer.exception.ContactNotFoundException;
import com.reece.customer.repository.ContactRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.time.OffsetDateTime;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ContactService {

  private final CustomerService customerService;
  private final ContactRepository contactRepository;

  public Contact saveContact(Long customerId, ContactRequest contactRequest) {
    Customer customer = customerService.getCustomer(customerId);
    boolean exists = customer.getContacts().stream()
        .anyMatch(contact -> contact.getTag().equalsIgnoreCase(contactRequest.getTag()));
    Contact contact = buildContact(new ContactPK(customerId, contactRequest.getTag()), contactRequest, exists);

    return contactRepository.save(contact);
  }

  public List<Contact> saveContacts(Long customerId, ContactsRequest contactsRequest) {
    final Set<ContactPK> customerContacts = customerService.getCustomer(customerId).getContacts().stream()
        .map(Contact::getContactPK)
        .collect(Collectors.toSet());

    List<Contact> contacts = contactsRequest.getContactList().stream()
        .map(contactRequest -> {
          ContactPK contactPK = new ContactPK(customerId, contactRequest.getTag());
          return buildContact(contactPK, contactRequest, customerContacts.contains(contactPK));
        })
        .sorted(Comparator.comparing(Contact::getCustomerId).thenComparing(Contact::getTag))
        .collect(Collectors.toList());

    return contactRepository.saveAll(contacts);
  }

  public Set<String> getContacts(int pageIndex, int pageSize) {
    Pageable pageable = PageRequest.of(pageIndex, pageSize, Sort.by("phoneNumber"));
    return contactRepository.findAll(pageable).stream()
        .map(Contact::getPhoneNumber)
        .collect(Collectors.toSet());
  }

  public void deleteContact(Long customerId, String tag) {
    Contact contact = contactRepository.findById(new ContactPK(customerId, tag)).orElseThrow(() ->
        new ContactNotFoundException(String.format("Cannot find contact with customerId: %s and tag: %s",
            customerId, tag)));

    contactRepository.delete(contact);
  }

  private Contact buildContact(ContactPK contactPK, ContactRequest contactRequest, boolean exists) {
    Contact.ContactBuilder contactBuilder = Contact.builder()
        .customerId(contactPK.getCustomerId())
        .tag(contactPK.getTag())
        .phoneNumber(contactRequest.getPhoneNumber());

    if (exists) {
      contactBuilder.updatedAt(OffsetDateTime.now());
    } else {
      contactBuilder.createdAt(OffsetDateTime.now());
    }

    return contactBuilder.build();
  }

}
