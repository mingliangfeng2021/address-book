package com.reece.customer.controller;

import com.reece.customer.api.contact.ContactRequest;
import com.reece.customer.api.contact.ContactsRequest;
import com.reece.customer.domain.Contact;
import com.reece.customer.service.ContactService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Callable;

@Validated
@RestController
@RequestMapping("/v1/customers")
@RequiredArgsConstructor
public class ContactController {

  @Value("${service.contact.maxPageSize}")
  private int contactMaxPageSize;

  private final ContactService contactService;

  @PutMapping("/{customerId}/contact")
  public Callable<ResponseEntity<Contact>> saveContact(@PathVariable Long customerId,
                                                       @RequestBody @Valid ContactRequest contactRequest) {
    return () -> {
      Contact contact = contactService.saveContact(customerId, contactRequest);
      return ResponseEntity.ok(contact);
    };
  }

  @PutMapping("/{customerId}/contacts")
  public Callable<ResponseEntity<List<Contact>>> saveContacts(@PathVariable Long customerId,
                                                              @RequestBody @Valid ContactsRequest contactsRequest) {
    return () -> {
      List<Contact> contacts = contactService.saveContacts(customerId, contactsRequest);
      return ResponseEntity.ok(contacts);
    };
  }

  @GetMapping("/contacts")
  public Callable<ResponseEntity<Set<String>>> getContacts(@RequestParam(value = "page", required = false) Integer page,
                                                           @RequestParam(value = "size", required = false) Integer size) {
    return () -> {
      int pageIndex = page == null ? 0 : page;
      int pageSize = size == null ? contactMaxPageSize : Math.min(size, contactMaxPageSize);
      Set<String> contacts = contactService.getContacts(pageIndex, pageSize);
      return ResponseEntity.ok(contacts);
    };
  }

  @DeleteMapping("/{customerId}/contacts/{tag}")
  public Callable<ResponseEntity<Void>> deleteContact(@PathVariable Long customerId,
                                                      @PathVariable String tag) {
    return () -> {
      contactService.deleteContact(customerId, tag);
      return ResponseEntity.ok().build();
    };
  }

}
