package com.reece.customer.controller;

import com.reece.customer.api.customer.CustomerRequest;
import com.reece.customer.domain.Customer;
import com.reece.customer.service.CustomerService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.Callable;

@Validated
@RestController
@RequestMapping("/v1/customers")
@RequiredArgsConstructor
public class CustomerController {

  private final CustomerService customerService;

  @PostMapping
  public Callable<ResponseEntity<Customer>> createCustomer(@RequestBody @Valid CustomerRequest customerRequest) {
    return () -> {
      Customer customer = customerService.createCustomer(customerRequest);
      return ResponseEntity.ok(customer);
    };
  }

  @PutMapping("/{customerId}")
  public Callable<ResponseEntity<Customer>> updateCustomer(@PathVariable Long customerId,
                                                           @RequestBody @Valid CustomerRequest customerRequest) {
    return () -> {
      Customer customer = customerService.updateCustomer(customerId, customerRequest);
      return ResponseEntity.ok(customer);
    };
  }

  @GetMapping("/{customerId}")
  public Callable<ResponseEntity<Customer>> getCustomer(@PathVariable Long customerId) {
    return () -> {
      Customer customer = customerService.getCustomer(customerId);
      return ResponseEntity.ok(customer);
    };
  }

  @DeleteMapping("/{customerId}")
  public Callable<ResponseEntity<Void>> deleteCustomer(@PathVariable Long customerId) {
    return () -> {
      customerService.deleteCustomer(customerId);
      return ResponseEntity.ok().build();
    };
  }

}
