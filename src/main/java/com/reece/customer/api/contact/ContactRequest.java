package com.reece.customer.api.contact;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ContactRequest {

  @NotBlank
  @Size(min = 1, max = 64)
  private String tag;

  @NotBlank
  @Size(min = 6, max = 32)
  private String phoneNumber;

}
