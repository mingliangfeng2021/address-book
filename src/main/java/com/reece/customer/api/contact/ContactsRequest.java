package com.reece.customer.api.contact;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ContactsRequest {

  @NotEmpty
  private List<@Valid ContactRequest> contactList;

}
