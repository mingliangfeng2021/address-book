package com.reece.customer.api;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class ApiError {

  private final String status;
  private final String message;

}
