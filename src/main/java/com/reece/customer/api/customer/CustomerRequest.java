package com.reece.customer.api.customer;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Getter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerRequest {

  @NotBlank
  @Size(min = 1, max = 128)
  private String firstName;

  @NotBlank
  @Size(min = 1, max = 128)
  private String lastName;

}
