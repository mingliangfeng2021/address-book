package com.reece.customer.repository;

import com.reece.customer.domain.Contact;
import com.reece.customer.domain.ContactPK;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ContactRepository extends JpaRepository<Contact, ContactPK> {

}
