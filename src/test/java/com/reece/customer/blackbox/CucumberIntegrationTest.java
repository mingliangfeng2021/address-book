package com.reece.customer.blackbox;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
  features = "src/test/resources/features",
  extraGlue = "com.reece.customer.blackbox"
)
public class CucumberIntegrationTest {

}