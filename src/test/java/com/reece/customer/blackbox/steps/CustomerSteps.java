package com.reece.customer.blackbox.steps;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import com.reece.customer.blackbox.common.ScenarioContext;
import com.reece.customer.domain.Customer;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class CustomerSteps {

  @Autowired
  private ScenarioContext scenarioContext;

  private final OpenApiValidationFilter openApiValidationFilter = new OpenApiValidationFilter("customer_v1.yml");

  @When("^a client created a customer$")
  public void createCustomer() {
    Customer customer = given()
        .contentType(ContentType.JSON.toString()).body("{ \"firstName\": \"someone\", \"lastName\": \"feng\" }")
        .filter(openApiValidationFilter)
        .when()
        .post(scenarioContext.getCustomerUrl())
        .then()
        .assertThat().statusCode(200)
        .and()
        .extract().as(Customer.class);

    scenarioContext.customer = customer;
  }

  @When("^the client updated that customer$")
  public void updateCustomer() {
    given()
        .contentType(ContentType.JSON.toString()).body("{ \"firstName\": \"someone-else\", \"lastName\": \"feng\" }")
        .filter(openApiValidationFilter)
        .when()
        .put(scenarioContext.getCustomerUrl(scenarioContext.customer.getId()))
        .then()
        .assertThat().statusCode(200);
  }

  @When("^the client deleted that customer$")
  public void deleteCustomer() {
    given()
        .filter(openApiValidationFilter)
        .when()
        .delete(scenarioContext.getCustomerUrl(scenarioContext.customer.getId()))
        .then()
        .assertThat().statusCode(200);
  }

  @Then("^the client can fetch the customer$")
  public void fetchCustomer() {
    Customer customer = scenarioContext.callGetCustomerApi();

    assertThat(customer.getFirstName()).isEqualTo("someone");
    assertThat(customer.getLastName()).isEqualTo("feng");
  }

  @Then("^the client can fetch the updated customer$")
  public void fetchUpdatedCustomer() {
    Customer customer = scenarioContext.callGetCustomerApi();

    assertThat(customer.getFirstName()).isEqualTo("someone-else");
    assertThat(customer.getLastName()).isEqualTo("feng");
  }

  @Then("^the client will get 404 when fetch the deleted customer$")
  public void errorWhenFetchCustomer() {
    given()
        .filter(openApiValidationFilter)
        .when()
        .get(scenarioContext.getCustomerUrl(scenarioContext.customer.getId()))
        .then()
        .assertThat().statusCode(404);
  }

}
