package com.reece.customer.blackbox.steps;

import com.atlassian.oai.validator.restassured.OpenApiValidationFilter;
import com.reece.customer.blackbox.common.ScenarioContext;
import com.reece.customer.domain.Contact;
import com.reece.customer.domain.ContactPK;
import com.reece.customer.domain.Customer;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.http.ContentType;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Arrays;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;

public class ContactSteps {

  @Autowired
  private ScenarioContext scenarioContext;

  private final OpenApiValidationFilter openApiValidationFilter = new OpenApiValidationFilter("contact_v1.yml");

  @When("^the client saved a contact for the customer")
  public void createContact() {
    Contact contact = given()
        .contentType(ContentType.JSON.toString()).body("{ \"tag\": \"mobile\", \"phoneNumber\": \"0425376902\" }")
        .filter(openApiValidationFilter)
        .when()
        .put(scenarioContext.getContactUrl(scenarioContext.customer.getId()))
        .then()
        .assertThat().statusCode(200)
        .and()
        .extract().as(Contact.class);

    scenarioContext.contact = contact;
  }

  @When("^the client saved contacts for the customer")
  public void createContacts() {
    Contact[] contacts = given()
        .contentType(ContentType.JSON.toString()).body("{ \"contactList\": [ { \"tag\": \"mobile\", \"phoneNumber\": \"0400000000\" }, { \"tag\": \"home\", \"phoneNumber\": \"0411111111\" } ] }")
        .filter(openApiValidationFilter)
        .when()
        .put(scenarioContext.getContactsUrl(scenarioContext.customer.getId()))
        .then()
        .assertThat().statusCode(200)
        .and()
        .extract().as(Contact[].class);

    scenarioContext.contacts = Arrays.asList(contacts);
  }

  @When("^the client updated the contact$")
  public void updateContact() {
    given()
        .contentType(ContentType.JSON.toString()).body("{ \"tag\": \"mobile\", \"phoneNumber\": \"0400000000\" }")
        .filter(openApiValidationFilter)
        .when()
        .put(scenarioContext.getContactUrl(scenarioContext.customer.getId()))
        .then()
        .assertThat().statusCode(200);
  }

  @When("^the client deleted that contact$")
  public void deleteContact() {
    given()
        .filter(openApiValidationFilter)
        .when()
        .delete(scenarioContext.getContactsUrl(scenarioContext.customer.getId(), "mobile"))
        .then()
        .assertThat().statusCode(200);
  }

  @Then("^the client should see the contact for the customer$")
  public void fetchCustomerForContact() {
    Customer customer = scenarioContext.callGetCustomerApi();

    Set<ContactPK> contactPKSet = customer.getContacts().stream()
        .map(Contact::getContactPK)
        .collect(Collectors.toSet());
    assertThat(contactPKSet).contains(new ContactPK(scenarioContext.customer.getId(), "mobile"));
  }

  @Then("^the client should see the contacts for the customer$")
  public void fetchCustomerForContacts() {
    Customer customer = scenarioContext.callGetCustomerApi();

    Set<ContactPK> contactPKSet = customer.getContacts().stream()
        .map(Contact::getContactPK)
        .collect(Collectors.toSet());
    assertThat(contactPKSet).contains(new ContactPK(scenarioContext.customer.getId(), "mobile"));
    assertThat(contactPKSet).contains(new ContactPK(scenarioContext.customer.getId(), "home"));
  }

  @Then("^the client should see the updated contact for the customer$")
  public void fetchCustomerForUpdatedContact() {
    Customer customer = scenarioContext.callGetCustomerApi();

    Optional<Contact> contactOptional = customer.getContacts().stream()
        .filter(contact -> contact.getTag().equals("mobile"))
        .findAny();
    assertThat(contactOptional).isPresent();
    assertThat(contactOptional.get().getPhoneNumber()).isEqualTo("0400000000");
  }

  @Then("^the client should not see the contact under the customer$")
  public void fetchCustomerForDeletedContact() {
    Customer customer = scenarioContext.callGetCustomerApi();

    Optional<Contact> contactOptional = customer.getContacts().stream()
        .filter(contact -> contact.getTag().equals("mobile"))
        .findAny();
    assertThat(contactOptional).isNotPresent();
  }

  @Then("^the client should be able to see all contacts$")
  public void fetchAllContacts() {
    String[] contacts = given()
        .filter(openApiValidationFilter)
        .when()
        .get(scenarioContext.getContactsUrl())
        .then()
        .assertThat().statusCode(200)
        .and()
        .extract().as(String[].class);
    assertThat(contacts).contains("0400000000");
    assertThat(contacts).contains("0411111111");
  }

}
