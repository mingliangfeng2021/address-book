package com.reece.customer.blackbox.common;

import com.reece.customer.domain.Contact;
import com.reece.customer.domain.Customer;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.List;

import static io.cucumber.spring.CucumberTestContext.SCOPE_CUCUMBER_GLUE;
import static io.restassured.RestAssured.given;

@Component
@Scope(SCOPE_CUCUMBER_GLUE)
public class ScenarioContext {

  public static final String SERVER_URL = "http://localhost";

  public static final String V1_CUSTOMERS = "/v1/customers";

  public static final String V1_CONTACT = "/v1/customers/%s/contact";
  public static final String V1_CONTACTS = "/v1/customers/%s/contacts";
  public static final String V1_ALL_CONTACTS = "/v1/customers/contacts";

  @LocalServerPort
  private int port;

  public Customer customer;

  public Contact contact;
  public List<Contact> contacts;

  public String getCustomerUrl() {
    return SERVER_URL + ":" + port + V1_CUSTOMERS;
  }

  public String getCustomerUrl(long id) {
    return getCustomerUrl() + "/" + id;
  }

  public String getContactUrl(long customerId) {
    return SERVER_URL + ":" + port + String.format(V1_CONTACT, customerId);
  }

  public String getContactsUrl() {
    return SERVER_URL + ":" + port + V1_ALL_CONTACTS;
  }

  public String getContactsUrl(long customerId) {
    return SERVER_URL + ":" + port + String.format(V1_CONTACTS, customerId);
  }

  public String getContactsUrl(long customerId, String tag) {
    return SERVER_URL + ":" + port + String.format(V1_CONTACTS, customerId) + "/" + tag;
  }

  public Customer callGetCustomerApi() {
    return given()
        .when()
        .get(getCustomerUrl(customer.getId()))
        .then()
        .assertThat().statusCode(200)
        .and()
        .extract().as(Customer.class);
  }

}
