package com.reece.customer.component.controller;

import com.reece.customer.api.contact.ContactRequest;
import com.reece.customer.api.contact.ContactsRequest;
import com.reece.customer.exception.ContactNotFoundException;
import com.reece.customer.service.ContactService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.util.Collections;
import java.util.stream.Collectors;

import static com.reece.customer.fixture.ContactBuilder.buildContact;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith({SpringExtension.class, MockitoExtension.class})
public class ContactControllerTest {

  public static final String V1_CONTACT = "/v1/customers/999/contact";
  public static final String V1_CONTACTS = "/v1/customers/999/contacts";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private ContactService contactService;

  @Test
  public void saveContact_givenValidRequest_shouldCallContactServiceToSave() throws Exception {
    when(contactService.saveContact(eq(999L), ArgumentMatchers.any(ContactRequest.class))).thenAnswer((invocation -> {
      ContactRequest contactRequest = (ContactRequest) invocation.getArguments()[1];
      return buildContact(contactRequest, 999L);
    }));

    MvcResult mvcResult = mockMvc.perform(
        put(V1_CONTACT)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{ \"tag\": \"mobile\", \"phoneNumber\": \"0412345678\" }")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("customerId").value("999"))
        .andExpect(jsonPath("tag").value("mobile"))
        .andExpect(jsonPath("phoneNumber").value("0412345678"));
  }

  @Test
  public void saveContact_givenRequestWithoutPhoneNumber_shouldReturn400() throws Exception {
    mockMvc.perform(
            put(V1_CONTACT)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"tag\": \"mobile\" }")
        ).andDo(print())
        .andExpect(status().is(400))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(containsString("phoneNumber")))
        .andExpect(content().string(not(containsString("tag"))));
  }

  @Test
  public void saveContact_givenRequestWithTooLongTag_shouldReturn400() throws Exception {
    String json = String.format("{ \"tag\": \"%s\", \"phoneNumber\": \"0412345678\" }", "m".repeat(65));
    mockMvc.perform(
            put(V1_CONTACT)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
        ).andDo(print())
        .andExpect(status().is(400))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(not(containsString("phoneNumber"))))
        .andExpect(content().string(containsString("tag")));
  }

  @Test
  public void saveContacts_givenValidRequest_shouldCallContactServiceToSave() throws Exception {
    when(contactService.saveContacts(eq(999L), ArgumentMatchers.any(ContactsRequest.class))).thenAnswer((invocation -> {
      ContactsRequest contactsRequest = (ContactsRequest) invocation.getArguments()[1];
      return contactsRequest.getContactList().stream()
          .map(contactRequest -> buildContact(contactRequest, 999L))
          .collect(Collectors.toList());
    }));

    MvcResult mvcResult = mockMvc.perform(
        put(V1_CONTACTS)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{ \"contactList\": [ { \"tag\": \"mobile\", \"phoneNumber\": \"0412345678\" } ] }")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0].customerId").value("999"))
        .andExpect(jsonPath("$[0].tag").value("mobile"))
        .andExpect(jsonPath("$[0].phoneNumber").value("0412345678"));
  }

  @Test
  public void saveContacts_givenRequestWithEmptyContactList_shouldReturn400() throws Exception {
    mockMvc.perform(
            put(V1_CONTACTS)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"contactList\": [ ] }")
        ).andDo(print())
        .andExpect(status().is(400));
  }

  @Test
  public void getContacts_givenValidRequest_shouldCallContactService() throws Exception {
    when(contactService.getContacts(eq(0), eq(1000))).thenReturn(Collections.singleton("04253676902"));

    MvcResult mvcResult = mockMvc.perform(
        get("/v1/customers/contacts")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("$[0]").value("04253676902"));
  }

  @Test
  public void deleteContact_givenValidRequest_shouldCallContactService() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
        delete(V1_CONTACTS + "/mobile")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(emptyString()));

    verify(contactService, times(1)).deleteContact(eq(999L), eq("mobile"));
  }

  @Test
  public void deleteContact_givenNoContactFound_shouldReturn404() throws Exception {
    doThrow(new ContactNotFoundException("Cannot find contact with customerId: 999 and tag: mobile"))
        .when(contactService).deleteContact(999L, "mobile");

    MvcResult mvcResult = mockMvc.perform(
        delete(V1_CONTACTS + "/mobile")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().is(404))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value("NOT_FOUND"))
        .andExpect(jsonPath("message").value("Cannot find contact with customerId: 999 and tag: mobile"));
  }

}
