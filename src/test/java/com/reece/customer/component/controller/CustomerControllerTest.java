package com.reece.customer.component.controller;

import com.reece.customer.api.customer.CustomerRequest;
import com.reece.customer.exception.CustomerNotFoundException;
import com.reece.customer.service.CustomerService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import static com.reece.customer.fixture.CustomerBuilder.buildCustomer;
import static org.hamcrest.Matchers.*;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.request;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
@ExtendWith({SpringExtension.class, MockitoExtension.class})
public class CustomerControllerTest {

  public static final String V1_CUSTOMERS = "/v1/customers";

  @Autowired
  private MockMvc mockMvc;

  @MockBean
  private CustomerService customerService;

  @Test
  public void createCustomer_givenValidRequest_shouldCallCustomerServiceToSave() throws Exception {
    when(customerService.createCustomer(ArgumentMatchers.any(CustomerRequest.class))).thenAnswer((invocation -> {
      CustomerRequest customerRequest = (CustomerRequest) invocation.getArguments()[0];
      return buildCustomer(customerRequest, 1000L);
    }));

    MvcResult mvcResult = mockMvc.perform(
        post(V1_CUSTOMERS)
            .contentType(MediaType.APPLICATION_JSON)
            .content("{ \"firstName\": \"someone\", \"lastName\": \"feng\" }")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("id").value("1000"))
        .andExpect(jsonPath("firstName").value("someone"))
        .andExpect(jsonPath("lastName").value("feng"));
  }

  @Test
  public void createCustomer_givenRequestWithoutLastName_shouldReturn400() throws Exception {
    mockMvc.perform(
            post(V1_CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"firstName\": \"someone\" }")
        ).andDo(print())
        .andExpect(status().is(400))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(containsString("lastName")))
        .andExpect(content().string(not(containsString("firstName"))));
  }

  @Test
  public void createCustomer_givenRequestWithTooLongLastName_shouldReturn400() throws Exception {
    String lastName = "a".repeat(129);
    String json = String.format("{ \"firstName\": \"someone\", \"lastName\": \"%s\" }", lastName);
    mockMvc.perform(
            post(V1_CUSTOMERS)
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
        ).andDo(print())
        .andExpect(status().is(400))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(containsString("lastName")))
        .andExpect(content().string(not(containsString("firstName"))));
  }

  @Test
  public void updateCustomer_givenValidRequest_shouldCallCustomerServiceToUpdate() throws Exception {
    when(customerService.updateCustomer(eq(999L), ArgumentMatchers.any(CustomerRequest.class))).thenAnswer((invocation -> {
      CustomerRequest customerRequest = (CustomerRequest) invocation.getArguments()[1];
      return buildCustomer(customerRequest, 999L);
    }));

    MvcResult mvcResult = mockMvc.perform(
        put(V1_CUSTOMERS + "/999")
            .contentType(MediaType.APPLICATION_JSON)
            .content("{ \"firstName\": \"LeBron\", \"lastName\": \"James\" }")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("id").value("999"))
        .andExpect(jsonPath("firstName").value("LeBron"))
        .andExpect(jsonPath("lastName").value("James"));
  }

  @Test
  public void updateCustomer_givenRequestWithBlankFirstName_shouldReturn400() throws Exception {
    mockMvc.perform(
            put(V1_CUSTOMERS + "/999")
                .contentType(MediaType.APPLICATION_JSON)
                .content("{ \"firstName\": \"   \", \"lastName\": \"someone\" }")
        ).andDo(print())
        .andExpect(status().is(400))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(content().string(not(containsString("lastName"))))
        .andExpect(content().string(containsString("firstName")));
  }

  @Test
  public void getCustomer_givenValidRequest_shouldCallCustomerService() throws Exception {
    when(customerService.getCustomer(eq(999L))).thenReturn(
        buildCustomer(999L, "firstName", "lastName"));

    MvcResult mvcResult = mockMvc.perform(
        get(V1_CUSTOMERS + "/999")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("id").value("999"))
        .andExpect(jsonPath("firstName").value("firstName"))
        .andExpect(jsonPath("lastName").value("lastName"));
  }

  @Test
  public void getCustomer_givenNoCustomerFound_shouldReturn404() throws Exception {
    when(customerService.getCustomer(eq(999L))).thenThrow(
        new CustomerNotFoundException("Cannot find customer with customerId: 999"));

    MvcResult mvcResult = mockMvc.perform(
        get(V1_CUSTOMERS + "/999")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().is(404))
        .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        .andExpect(jsonPath("status").value("NOT_FOUND"))
        .andExpect(jsonPath("message").value("Cannot find customer with customerId: 999"));
  }

  @Test
  public void deleteCustomer_givenValidRequest_shouldCallCustomerService() throws Exception {
    MvcResult mvcResult = mockMvc.perform(
        delete(V1_CUSTOMERS + "/999")
    ).andExpect(request().asyncStarted()).andReturn();

    mockMvc.perform(asyncDispatch(mvcResult))
        .andDo(print())
        .andExpect(status().isOk())
        .andExpect(content().string(emptyString()));

    verify(customerService, times(1)).deleteCustomer(eq(999L));
  }

}
