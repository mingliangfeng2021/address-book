package com.reece.customer.component.repository;

import com.reece.customer.domain.Customer;
import com.reece.customer.repository.CustomerRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.reece.customer.fixture.CustomerBuilder.buildCustomer;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@SpringBootTest
@ExtendWith({SpringExtension.class, MockitoExtension.class})
public class CustomerRepositoryTest {

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private Customer customer1;
  private Customer customer2;

  @BeforeEach
  public void setup() {
    customer1 =  customerRepository.save(buildCustomer("LeBron", "James"));
    customer2 =  customerRepository.save(buildCustomer("Kevin", "Durant"));
  }

  @AfterEach
  public void tearDown() {
    customerRepository.deleteAll();
  }

  @Test
  public void save_givenNewEntity_shouldSave() {
    Customer customer = customerRepository.save(buildCustomer("someone", "feng"));

    assertThat(customer.getId()).isNotNull();
    assertThat(countById(customer.getId())).isEqualTo(1);
    assertThat(customer.getFirstName()).isEqualTo("someone");
    assertThat(customer.getLastName()).isEqualTo("feng");
  }

  @Test
  public void save_givenExistingEntity_shouldUpdate() {
    long customerId = customer1.getId();
    Customer customer = customerRepository.save(buildCustomer(customerId, "someone", "feng"));

    assertThat(customer.getId()).isEqualTo(customerId);
    assertThat(customer.getFirstName()).isEqualTo("someone");
    assertThat(customer.getLastName()).isEqualTo("feng");
  }

  @Test
  public void findById_shouldFetchDataFromDB() {
    long customerId = customer2.getId();

    assertThat(customerRepository.findById(customerId)).isPresent();
    assertThat(customerRepository.findById(Long.MAX_VALUE)).isNotPresent();
  }

  @Test
  public void delete_shouldDeleteRecordFromDB() {
    long customerId = customer1.getId();
    int count = countAll();

    customerRepository.delete(customerRepository.getById(customerId));

    assertThat(countAll()).isEqualTo(count - 1);
  }

  private int countById(long customerId) {
    return jdbcTemplate.queryForObject("select count(*) from customer where id = " + customerId, Integer.class);
  }

  private int countAll() {
    return jdbcTemplate.queryForObject("select count(*) from customer", Integer.class);
  }

}
