package com.reece.customer.component.service;

import com.reece.customer.repository.ContactRepository;
import com.reece.customer.repository.CustomerRepository;
import com.reece.customer.domain.Contact;
import com.reece.customer.domain.Customer;
import com.reece.customer.service.CustomerService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static com.reece.customer.fixture.ContactBuilder.buildContact;
import static com.reece.customer.fixture.CustomerBuilder.buildCustomer;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith({SpringExtension.class, MockitoExtension.class})
public class CustomerServiceTest {

  @Autowired
  private CustomerService customerService;

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private ContactRepository contactRepository;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private Customer customer;
  private Contact contact;

  @BeforeEach
  public void setup() {
    customer = customerRepository.save(buildCustomer("firstName", "lastName"));

    contact = contactRepository.save(buildContact(customer.getId(), "mobile", "0499998888"));
    contactRepository.save(buildContact(customer.getId(), "office", "0399998888"));
  }

  @AfterEach
  public void tearDown() {
    contactRepository.deleteAll();
    customerRepository.delete(customer);
  }

  @Test
  public void deleteCustomer_shouldDeleteCustomerAndAllContacts() {
    assertThat(countAllCustomers()).isEqualTo(1);
    assertThat(countAllContacts()).isEqualTo(2);

    customerService.deleteCustomer(customer.getId());

    assertThat(countAllCustomers()).isEqualTo(0);
    assertThat(countAllContacts()).isEqualTo(0);
  }

  private int countAllContacts() {
    return jdbcTemplate.queryForObject("select count(*) from contact", Integer.class);
  }

  private int countAllCustomers() {
    return jdbcTemplate.queryForObject("select count(*) from customer", Integer.class);
  }

  private Contact buildContactOnMars() {
    return buildContact(customer.getId(), "Mars", "0010001");
  }

  private Contact buildNewContact1() {
    return buildContact(contact.getCustomerId(), contact.getTag(), "0912345678");
  }

}
