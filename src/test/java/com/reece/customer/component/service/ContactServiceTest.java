package com.reece.customer.component.service;

import com.reece.customer.repository.ContactRepository;
import com.reece.customer.repository.CustomerRepository;
import com.reece.customer.domain.Contact;
import com.reece.customer.domain.ContactPK;
import com.reece.customer.domain.Customer;
import com.reece.customer.service.ContactService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Arrays;
import java.util.List;

import static com.reece.customer.fixture.ContactBuilder.buildContact;
import static com.reece.customer.fixture.CustomerBuilder.buildCustomer;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
@ExtendWith({SpringExtension.class, MockitoExtension.class})
public class ContactServiceTest {

  @Autowired
  private ContactService contactService;

  @Autowired
  private CustomerRepository customerRepository;

  @Autowired
  private ContactRepository contactRepository;

  @Autowired
  private JdbcTemplate jdbcTemplate;

  private Customer customer;
  private Contact contact;

  @BeforeEach
  public void setup() {
    customer = customerRepository.save(buildCustomer("firstName", "lastName"));

    contact = contactRepository.save(buildContact(customer.getId(), "mobile", "0499998888"));
    contactRepository.save(buildContact(customer.getId(), "office", "0399998888"));
  }

  @AfterEach
  public void tearDown() {
    contactRepository.deleteAll();
    customerRepository.delete(customer);
  }

  @Test
  public void existsById_shouldReportAccordingToDbRecord() {
    assertThat(contactRepository.existsById(new ContactPK(contact.getCustomerId(), contact.getTag()))).isTrue();
    assertThat(contactRepository.existsById(new ContactPK(Long.MAX_VALUE, contact.getTag()))).isFalse();
  }

  @Test
  public void save_givenNewEntity_shouldSave() {
    assertThat(countAll()).isEqualTo(2);
    Contact contact = contactRepository.save(buildContactOnMars());

    assertThat(countAll()).isEqualTo(3);
    assertThat(contact.getPhoneNumber()).isEqualTo("0010001");
  }

  @Test
  public void save_givenExistingEntity_shouldUpdate() {
    assertThat(countAll()).isEqualTo(2);
    Contact contact = contactRepository.save(buildNewContact1());

    assertThat(countAll()).isEqualTo(2);
    assertThat(contact.getPhoneNumber()).isEqualTo("0912345678");
  }

  @Test
  public void saveAll_shouldSaveOrUpdate() {
    List<Contact> contacts = Arrays.asList(buildNewContact1(), buildContactOnMars());

    assertThat(countAll()).isEqualTo(2);
    List<Contact> savedContacts = contactRepository.saveAll(contacts);

    assertThat(countAll()).isEqualTo(3);
    assertThat(savedContacts).hasSize(2);
  }

  @Test
  public void findAll_shouldFetchContacts() {
    Pageable pageable = PageRequest.of(0, 1, Sort.by("phoneNumber"));
    List<Contact> contacts = contactRepository.findAll(pageable).toList();

    assertThat(contacts).hasSize(1);
    assertThat(contacts.get(0).getPhoneNumber()).isEqualTo("0399998888");
  }

  @Test
  public void findById_shouldReportAccordingToDbRecord() {
    assertThat(contactRepository.findById(new ContactPK(contact.getCustomerId(), contact.getTag()))).isPresent();
    assertThat(contactRepository.findById(new ContactPK(Long.MAX_VALUE, contact.getTag()))).isNotPresent();
  }

  @Test
  public void delete_shouldDeleteRecordFromDB() {
    ContactPK contactPK = contact.getContactPK();
    assertThat(contactRepository.findById(contactPK)).isPresent();

    contactRepository.delete(contactRepository.getById(contactPK));

    assertThat(contactRepository.findById(contactPK)).isNotPresent();
  }

  private int countAll() {
    return jdbcTemplate.queryForObject("select count(*) from contact", Integer.class);
  }

  private Contact buildContactOnMars() {
    return buildContact(customer.getId(), "Mars", "0010001");
  }

  private Contact buildNewContact1() {
    return buildContact(contact.getCustomerId(), contact.getTag(), "0912345678");
  }

}
