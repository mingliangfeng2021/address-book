package com.reece.customer.unit.service;

import com.reece.customer.api.customer.CustomerRequest;
import com.reece.customer.repository.ContactRepository;
import com.reece.customer.domain.Customer;
import com.reece.customer.exception.CustomerNotFoundException;
import com.reece.customer.repository.CustomerRepository;
import com.reece.customer.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class CustomerServiceTest {

  private CustomerService customerService;

  @Mock
  private CustomerRepository customerRepository;

  @Mock
  private ContactRepository contactRepository;

  @Captor
  private ArgumentCaptor<Customer> customerArgumentCaptor;

  @BeforeEach
  public void setup() {
    customerService = new CustomerService(customerRepository, contactRepository);
  }

  @Test
  public void saveCustomer_shouldBuildCustomerAndCallCustomerRepositoryToSave() {
    customerService.createCustomer(new CustomerRequest("someone", "feng"));

    verify(customerRepository).save(customerArgumentCaptor.capture());
    Customer customer = customerArgumentCaptor.getValue();
    assertThat(customer.getFirstName()).isEqualTo("someone");
    assertThat(customer.getLastName()).isEqualTo("feng");
    assertThat(customer.getCreatedAt()).isNotNull();
    assertThat(customer.getUpdatedAt()).isNull();
  }

  @Test
  public void updateCustomer_shouldBuildCustomerAndCallCustomerRepositoryToSave() {
    when(customerRepository.findById(999L)).thenReturn(Optional.of(new Customer()));

    customerService.updateCustomer(999L, new CustomerRequest("someone", "feng"));

    verify(customerRepository).save(customerArgumentCaptor.capture());
    Customer customer = customerArgumentCaptor.getValue();
    assertThat(customer.getFirstName()).isEqualTo("someone");
    assertThat(customer.getLastName()).isEqualTo("feng");
    assertThat(customer.getUpdatedAt()).isNotNull();
  }

  @Test
  public void getCustomer_shouldCallCustomerRepository() {
    Customer mockCustomer = mock(Customer.class);
    when(customerRepository.findById(999L)).thenReturn(Optional.of(mockCustomer));

    Customer customer = customerService.getCustomer(999L);
    assertThat(customer).isEqualTo(mockCustomer);
  }

  @Test
  public void getCustomer_givenCustomerNotFound_shouldThrowException() {
    when(customerRepository.findById(999L)).thenReturn(Optional.empty());

    assertThatThrownBy(() -> customerService.getCustomer(999L))
        .isInstanceOf(CustomerNotFoundException.class)
        .hasMessageContaining("Cannot find customer");
  }

  @Test
  public void deleteCustomer_shouldCallCustomerRepository() {
    Customer mockCustomer = mock(Customer.class);
    when(customerRepository.findById(999L)).thenReturn(Optional.of(mockCustomer));

    customerService.deleteCustomer(999L);

    verify(customerRepository).delete(customerArgumentCaptor.capture());
    Customer customer = customerArgumentCaptor.getValue();
    assertThat(customer).isEqualTo(mockCustomer);
  }

}
