package com.reece.customer.unit.service;

import com.reece.customer.api.contact.ContactRequest;
import com.reece.customer.api.contact.ContactsRequest;
import com.reece.customer.domain.Contact;
import com.reece.customer.domain.ContactPK;
import com.reece.customer.exception.ContactNotFoundException;
import com.reece.customer.repository.ContactRepository;
import com.reece.customer.service.ContactService;
import com.reece.customer.service.CustomerService;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Pageable;

import java.util.*;

import static com.reece.customer.fixture.ContactBuilder.*;
import static com.reece.customer.fixture.CustomerBuilder.buildCustomer;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class ContactServiceTest {

  private static final long CUSTOMER_ID = 999L;
  private static final String PHONE_NUMBER = "0412345678";

  private ContactService contactService;

  @Mock
  private ContactRepository contactRepository;

  @Mock
  private CustomerService customerService;

  @Captor
  private ArgumentCaptor<Contact> contactArgumentCaptor;

  @Captor
  private ArgumentCaptor<List<Contact>> contactsArgumentCaptor;

  @BeforeEach
  public void setup() {
    contactService = new ContactService(customerService, contactRepository);
  }

  @Test
  public void saveContact_givenNewContact_shouldBuildAndCallContactRepositoryToSave() {
    String tag = "mobile";
    when(customerService.getCustomer(CUSTOMER_ID)).thenReturn(buildCustomer(CUSTOMER_ID));

    contactService.saveContact(CUSTOMER_ID, new ContactRequest(tag, PHONE_NUMBER));

    verify(contactRepository).save(contactArgumentCaptor.capture());
    Contact contact = contactArgumentCaptor.getValue();
    assertThat(contact.getPhoneNumber()).isEqualTo(PHONE_NUMBER);
    assertThat(contact.getCreatedAt()).isNotNull();
    assertThat(contact.getUpdatedAt()).isNull();
  }

  @Test
  public void saveContact_givenNewContact_shouldBuildAndCallContactRepositoryToUpdate() {
    String tag = "mobile";
    when(customerService.getCustomer(CUSTOMER_ID)).thenReturn(buildCustomer(CUSTOMER_ID, tag));

    contactService.saveContact(CUSTOMER_ID, new ContactRequest(tag, PHONE_NUMBER));

    verify(contactRepository).save(contactArgumentCaptor.capture());
    Contact contact = contactArgumentCaptor.getValue();
    assertThat(contact.getPhoneNumber()).isEqualTo(PHONE_NUMBER);
    assertThat(contact.getUpdatedAt()).isNotNull();
  }

  @Test
  public void saveContacts_givenMultipleContactRequests_shouldBuildAndSortAndCallContactRepositoryToSave() {
    when(customerService.getCustomer(CUSTOMER_ID)).thenReturn(buildCustomer(CUSTOMER_ID));

    ContactsRequest contactsRequest = buildContactsRequest();
    contactService.saveContacts(CUSTOMER_ID, contactsRequest);

    verify(contactRepository).saveAll(contactsArgumentCaptor.capture());
    List<Contact> contacts = contactsArgumentCaptor.getValue();
    assertThat(contacts).hasSize(2);

    assertThat(contacts.get(0).getTag()).isEqualTo("a");
    assertThat(contacts.get(0).getPhoneNumber()).isEqualTo("0312345678");
    assertThat(contacts.get(0).getUpdatedAt()).isNotNull();

    assertThat(contacts.get(1).getTag()).isEqualTo("b");
    assertThat(contacts.get(1).getPhoneNumber()).isEqualTo("0412345678");
    assertThat(contacts.get(1).getCreatedAt()).isNotNull();
    assertThat(contacts.get(1).getUpdatedAt()).isNull();
  }

  @Test
  public void getContacts_shouldCallContactRepositoryToQuery() {
    when(contactRepository.findAll(any(Pageable.class))).thenReturn(buildContactList(CUSTOMER_ID));

    Set<String> phoneNumbers = contactService.getContacts(0, 100);

    assertThat(phoneNumbers).hasSize(1);
    assertThat(phoneNumbers).contains("0499998888");
  }

  @Test
  public void deleteContact_shouldCallContactRepository() {
    String tag = "mobile";
    ContactPK contactPK = new ContactPK(CUSTOMER_ID, tag);
    when(contactRepository.findById(contactPK))
        .thenReturn(Optional.of(buildContact(CUSTOMER_ID, tag, PHONE_NUMBER)));

    contactService.deleteContact(CUSTOMER_ID, tag);

    verify(contactRepository).delete(contactArgumentCaptor.capture());
    Contact contact = contactArgumentCaptor.getValue();
    assertThat(contact.getContactPK()).isEqualTo(contactPK);
  }

  @Test
  public void deleteContact_givenContactNotFound_shouldThrowException() {
    String tag = "mobile";
    when(contactRepository.findById(new ContactPK(CUSTOMER_ID, tag))).thenReturn(Optional.empty());

    assertThatThrownBy(() -> contactService.deleteContact(CUSTOMER_ID, tag))
        .isInstanceOf(ContactNotFoundException.class)
        .hasMessageContaining("Cannot find contact");
  }

}
