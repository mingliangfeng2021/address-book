package com.reece.customer.fixture;

import com.reece.customer.api.customer.CustomerRequest;
import com.reece.customer.domain.Contact;
import com.reece.customer.domain.Customer;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.Collections;

import static com.reece.customer.fixture.ContactBuilder.buildContact;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class CustomerBuilder {

  public static Customer buildCustomer(long customerId) {
    return buildCustomer(customerId, "a");
  }

  public static Customer buildCustomer(long customerId, String tag) {
    Contact contact = buildContact(customerId, tag, "0499998888");

    Customer customer = new Customer();
    customer.setId(customerId);
    customer.setContacts(Collections.singletonList(contact));

    return customer;
  }

  public static Customer buildCustomer(String fistName, String lastName) {
    Customer customer = new Customer();
    customer.setFirstName(fistName);
    customer.setLastName(lastName);

    return customer;
  }

  public static Customer buildCustomer(long customerId, String fistName, String lastName) {
    Customer customer = buildCustomer(fistName, lastName);
    customer.setId(customerId);

    return customer;
  }

  public static Object buildCustomer(CustomerRequest customerRequest, long customerId) {
    return Customer.builder()
        .id(customerId)
        .firstName(customerRequest.getFirstName())
        .lastName(customerRequest.getLastName())
        .build();
  }

}
