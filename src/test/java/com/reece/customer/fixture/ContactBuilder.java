package com.reece.customer.fixture;

import com.reece.customer.api.contact.ContactRequest;
import com.reece.customer.api.contact.ContactsRequest;
import com.reece.customer.domain.Contact;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.data.domain.PageImpl;

import java.util.Arrays;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ContactBuilder {

  public static Contact buildContact(long customerId, String tag, String phoneNumber) {
    Contact contact = buildContact(tag, phoneNumber);
    contact.setCustomerId(customerId);
    return contact;
  }

  public static Contact buildContact(String tag, String phoneNumber) {
    Contact contact = new Contact();
    contact.setTag(tag);
    contact.setPhoneNumber(phoneNumber);
    return contact;
  }

  public static Contact buildContact(ContactRequest contactRequest, long customerId) {
    return Contact.builder()
        .customerId(customerId)
        .tag(contactRequest.getTag())
        .phoneNumber(contactRequest.getPhoneNumber())
        .build();
  }

  public static ContactsRequest buildContactsRequest() {
    return new ContactsRequest(Arrays.asList(
        new ContactRequest("b", "0412345678"),
        new ContactRequest("a", "0312345678")
    ));
  }

  public static PageImpl<Contact> buildContactList(long customerId) {
    return new PageImpl(Arrays.asList(
        buildContact(customerId, "a", "0499998888"),
        buildContact(customerId, "b", "0499998888")
    ));
  }

}
