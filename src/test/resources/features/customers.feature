Feature: customers

  Scenario: create customer
    When a client created a customer
    Then the client can fetch the customer

  Scenario: update customer
    When a client created a customer
    And the client updated that customer
    Then the client can fetch the updated customer

  Scenario: delete customer
    When a client created a customer
    And the client deleted that customer
    Then the client will get 404 when fetch the deleted customer
