Feature: contacts

  Scenario: save contact
    When a client created a customer
    And the client saved a contact for the customer
    Then the client should see the contact for the customer

  Scenario: save contacts
    When a client created a customer
    And the client saved contacts for the customer
    Then the client should see the contacts for the customer

  Scenario: update contact
    When a client created a customer
    And the client saved a contact for the customer
    And the client updated the contact
    Then the client should see the updated contact for the customer

  Scenario: delete contact
    When a client created a customer
    And the client saved a contact for the customer
    And the client deleted that contact
    Then the client should not see the contact under the customer

  Scenario: fetch contacts
    When a client created a customer
    And the client saved contacts for the customer
    Then the client should be able to see all contacts
