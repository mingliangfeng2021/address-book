git clone https://mingliangfeng@bitbucket.org/mingliangfeng2021/address-book.git  
cd address-book

./gradlew clean build

./gradlew cucumber

./gradlew bootRun

> **or build docker image and run container**
>> ./gradlew jibDockerBuild -Djib.container.creationTime=USE_CURRENT_TIMESTAMP  
>> docker run -p 8080:8080 --name address-book address-book:1.0-SNAPSHOT

```

echo '\n\ncreate customer\n'
curl -i -X POST http://localhost:8080/v1/customers \
-H 'Content-Type: application/json' \
-d '{ "firstName": "someone", "lastName": "feng" }'

echo '\n\nget customer\n'
curl -i http://localhost:8080/v1/customers/1

echo '\n\nupdate customer\n'
curl -i -X PUT http://localhost:8080/v1/customers/1 \
-H 'Content-Type: application/json' \
-d '{ "firstName": "someone-else", "lastName": "Wang" }'

echo '\n\ncreate contact\n'
curl -i -X PUT http://localhost:8080/v1/customers/1/contact \
-H 'Content-Type: application/json' \
-d '{ "tag": "mobile", "phoneNumber": "0425376902" }'

echo '\n\nsave more contacts\n'
curl -i -X PUT http://localhost:8080/v1/customers/1/contacts \
-H 'Content-Type: application/json' \
-d '{ "contactList": [ { "tag": "mobile", "phoneNumber": "0400000000" }, { "tag": "home", "phoneNumber": "0411111111" } ] }'

echo '\n\nget all contacts\n'
curl -i http://localhost:8080/v1/customers/contacts

echo '\n\ndelete a contact\n'
curl -i -X DELETE http://localhost:8080/v1/customers/1/contacts/mobile

echo '\n\nget all contacts\n'
curl -i http://localhost:8080/v1/customers/contacts

echo '\n\ndelete customer\n'
curl -i -X DELETE http://localhost:8080/v1/customers/1

echo '\n\nget all contacts\n'
curl -i http://localhost:8080/v1/customers/contacts

```